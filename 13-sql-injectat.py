# /usr/bin/python
#-*- coding: utf-8-*-
#
# --------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# --------------------------------------

# exemple-popen.py
#---------------------------------------
from subprocess import Popen, PIPE
import sys, argparse

parser = argparse.ArgumentParser(description="Consulta sql")
parser.add_argument("sqlStatment", metavar="sentenciaSQL", help="sentencia sql a executar")
args=parser.parse_args()

consulta=[f"psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"{args.sqlStatment}\""]
pipeData = Popen(consulta,stdout=PIPE, shell=True)
for line in pipeData.stdout:
	print(line.decode("utf-8"),end="")
exit(0)