# /usr/bin/python3
#-*- coding: utf-8-*-
# telnet-client
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2019
# -------------------------------------
#
# Pere Canet Pons
#
import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Telnet server""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()

HOST = args.server
PORT = args.port
EOF = bytes(chr(4), 'utf-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
    command = input("~$ ")
    if not command: break
    s.sendall(bytes(command, 'utf-8'))
    while True:
        data = s.recv(1024)
        if data[-1:] == EOF:
            print('Rebut', repr(data[:-1])) #printar tot fins el EOF, per si la linia conté més coses
            break
        print('Rebut', repr(data))

s.close()
sys.exit(0)
