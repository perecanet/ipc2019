# exemple-signal.py
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, os, signal

def myhola(signum, frame):
    print("hola radiola")

def myadeu(signum, frame):
    print("adeu andreu")

pid=os.fork()
if pid != 0:
    print("Pid fill", pid)
    exit(0)

signal.signal(signal.SIGUSR1,myhola) # 10
signal.signal(signal.SIGUSR2,myadeu) # 12

while True:
    pass
