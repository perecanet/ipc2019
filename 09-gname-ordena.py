# /usr/bin/python
#-*- coding: utf-8-*-
#
# sort-users [-s gecos]  file

import sys, argparse
from functools import cmp_to_key
groupDict={}
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,\
        help="sort criteria: gname", metavar="criteria",\
        choices=["gname"],dest="criteria")
parser.add_argument("-u","--userFile",type=str,\
        help="user file (/etc/passwd style)", metavar="userFile")
parser.add_argument("-g","--groupFile",type=str,\
        help="user file (/etc/passwd style)", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupDict:
      self.gname=groupDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
  def __str__(self):
    "function to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
class UnixGroup():
  """Classe grup de unix: prototipus /etc/group
  gname:passwd:gid:listusers"""
  def __init__(self,groupLine):
    "constructor de un unixgroup donada una línia del /etc/group"
    groupField = groupLine[:-1].split(":")
    self.gname = groupLine[0]
    self.passwd = groupLine[1]
    self.gid = groupLine[2]
    self.userListStr = groupLine[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "function to string"
    return "%s %d %s" %(self.gname, int(self.gid), self.userList)
# -------------------------------------------------------
def sort_gname(user):
  '''Compara segons gname'''
  return (user.gname, user.login)
#--------------------------------------------------------
fileIn=open("copia_group","r")
for line in fileIn:
  oneGroup=UnixGroup(line)
  groupDict[oneGroup.gid]=oneGroup
fileIn.close()
# -------------------------------------------------------
fileIn=open("copia_passwd","r")
userList=[]
for line in fileIn:
  oneUser=UnixUser(line)
  userList.append(oneUser)
fileIn.close()
print(userList[0])
#--------------------------------------------------------
if args.criteria=="gname":
  userList.sort(key=sort_gname)
for user in userList:
 print(user)
exit(0)
