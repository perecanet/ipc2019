#!/usr/bin/python3
# -*- coding:utf-8-*-
# 08-sort-users.py [-s login|gid] file
# Carregar en una llista a memòria els usuaris provinents d'un fitxer
# tipus /etc/passwd, usant objectes *UnixUser*, i llistar-los.
# Ordenar el llistat (stdout) segons el criteri login o el criteri 
# gid (estable).

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s","--sort",type=str,help="métode d'ordenació",\
		dest="metode",metavar="metodesort",choices=["login","gid"],default="gid")
parser.add_argument("-f","--file",type=str,\
        required=True,help="fitxer a processar", metavar="file")
args=parser.parse_args()
print(args)

class UnixUser():
	""" 
	Classe UnixUser: prototipus de /etc/passwd
	   login:passwd:uid:gid:gecos:home:shell
	"""
	def __init__(self, user_linea):
		"Constructor objectes UnixUser"
		camp = user_linea.split(':')
		self.login=camp[0]
		self.passwd=camp[1]
		self.uid=int(camp[2])
		self.gid=int(camp[3])
		self.gecos=camp[4]
		self.home=camp[5]
		self.shell=camp[6]
		
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login: %s uid: %d gid: %d" % (self.login,self.uid,self.gid))
		
	def sumaun(self):
		self.uid+=1
		
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %s %d %d %s %s %s" % (self.login,self.passwd,self.uid,self.gid,self.gecos,self.home,self.shell)

# funciones comparadoras
def sort_login(user):
	"""
	Comparador logins
	"""
	return user.login

def sort_gid(user):
	"""
	Comparador gids
	"""
	return user.gid

fileIn=open(args.file,'r')
listUsers=[]

# cargar usuarios en una lista
for linea in fileIn:
	user = UnixUser(linea)
	listUsers.append(user)
fileIn.close()

# ordenar según el criterio
if args.metode == "login":
	listUsers.sort(key=sort_login)
elif args.metode == "gid":
	listUsers.sort(key=sort_gid)

# recorrer esta lista y mostrarla
for user in listUsers:
    user.show()
