# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-signal.py
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, os, signal

def myhandler(signum,frame):
    print("Signal handler called with signal:", signum)
    print("adeu siau")
    sys.exit(1)

def mydeath(signum,frame):
    print("Signal handler called with signal:", signum)
    print("no morir")

signal.signal(signal.SIGALRM,myhandler) #14 associacio senyals
signal.signal(signal.SIGUSR2,myhandler) #12
signal.signal(signal.SIGUSR1,mydeath) #10
signal.signal(signal.SIGTERM,signal.SIG_IGN) #ignorar el sigterm i sigint
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.alarm(60)
print(os.getpid())

while True:
    pass
signal.alarm(0)
sys.exit(0)