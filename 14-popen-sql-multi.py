# /usr/bin/python
#-*- coding: utf-8-*-
#
# --------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# --------------------------------------

# exemple-popen.py
#---------------------------------------
from subprocess import Popen, PIPE
import sys, argparse

parser = argparse.ArgumentParser(description="Consulta sql")
parser.add_argument("-d", "--database", type=str, help="database", metavar="database",\
    required=True, dest="database")
parser.add_argument("-c", "--consulta", type=str, help="consulta", metavar="consulta",\
    dest="consulta")
args=parser.parse_args()

cmd = "psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training "
pipeData = Popen(cmd,shell=True, stdout=PIPE, stdin=PIPE, stderr=PIPEN)
pipeData.stdin.write(b"select * from oficinas; \n\q\n")
for line in pipeData.comunicate:
	print(line.decode("utf-8"),end="")
sys.exit(0)