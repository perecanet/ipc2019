# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-signal.py
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, os, signal, argparse
parser = argparse.ArgumentParser(description="Gestionar l'alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
global upp
global down
upp = 0
down = 0

def mysigusr1(signum,frame):
    global upp
    print("Signal handler called with signal:", signum)
    upp+=1
    actual=signal.alarm(0)
    signal.alarm(actual+60)

def myminuti(signum,frame):
    signal.alarm 
    print("1 min menys", signum)
def myrestore(signum,frame):
    signal.alarm(120)
    print("alarm 120 seg")
def mytime(signum,frame):
    print(signal.alarm())
def myfinish(signum,frame):
    print("temps acabat")
    exit(0)

signal.signal(signal.SIGHUP,myrestore) # restaura temps 1
signal.signal(signal.SIGUSR2,myminuti) # restar minut 12
signal.signal(signal.SIGUSR1,mymin) # sumar minut 10
signal.signal(signal.SIGTERM,mytime) #quant queda 15
signal.signal(signal.SIGINT,signal.SIG_IGN)
signal.signal(signal.SIGALRM,myfinish) #14

signal.alarm(20)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
