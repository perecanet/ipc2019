# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = '35.177.182.104'
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    if not data: break
    print(repr(data))
s.close()
sys.exit(0)
