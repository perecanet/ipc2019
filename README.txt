Alumne: Pere Canet Pons
HISX2
06/02/2020

Documentació del servidor creat:

IP amazon: 
Port per defecte: 44444
Arguments disponibles: -d/--debug ; -p/--port

Explicació funcionament:

En aquest servidor els arguments que es poden introduir són el -d/--debug (mode debug)
i el -p/--port (elecció de port). Aquest últim té un valor per defecte de 44444.

Com deia l'enunciat de l'examen, aquest servidor té configurats tres senyals:
 -El numero 10 mostra per pantalla el llistat de connexions i tanca.
 -El numero 12 mostra per pantalla el numero total de connexions i tanca.
 -El numero 15 és una fussió dels dos anteriors.

Així doncs, un exemple d'enviament de senyal seria executar la següent ordre:
        kill -[10|12|15] pid-servidor
        El pid del servidor es pot obtenir executant l'ordre ps ax

Per altra banda, un cop establerta la connexió amb el servidor, el client pot executar
tres comandes: processos, ports o qualsevol altre que serà interpretada com whoareyou.
    -Processos: s'executa la comanda ps ax
    -Ports: s'executa la comanda netstat -puta
    -Whoareyou/qualsevol altre: uname -a

Hi ha que anar en compte amb les errades ortogràfiques!

Per tancar la connexió del client es pot fer amb CTRL+C.

