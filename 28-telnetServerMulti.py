#!/usr/bin/python3
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisx2 M06-ASO UF2NF1-Scripts
# @edt Curs 2019-2020
# Gener 2020
# Telnet server multiple-connexions
# Pere Canet Pons
# 31/01/2020
# -----------------------------------------------------------------
'''
import sys,socket,argparse, signal, select, os
from subprocess import Popen, PIPE

HOST = '18.130.211.58'                 
PORT = 50007             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                pipeData = Popen(data, stdout=PIPE, stderr=PIPE, shell=True)
                for line in pipeData.stdout:
                    actual.sendall(line)
                for line in pipeData.stderr:
                    actual.sendall(line)
                actual.sendall(bytes(chr(4), 'utf-8'),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)