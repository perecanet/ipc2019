#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Els clients es connecten a un servidor, envien un informe consistent en fer
# "ps ax" i finalitzen la connexió. El servidor rep l'informe del client i
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on
# timestamp té el format AADDMM-HHMMSS.
#
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys, socket, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""client PS""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int,default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = "ps ax"
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
    s.send(line)
s.close()
sys.exit(0)