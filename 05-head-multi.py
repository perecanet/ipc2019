# /usr/bin/python
#-*- coding: utf-8-*-
#
# head [-n5/10/15] file
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# -------------------------------------
# $ head.py [-n 5/10/15] [-f file.txt]
# tots els altres casos d'error
# -------------------------------------
import sys, argparse

def mostra_linies(fitxer):

    fileIn=open(fitxer,'r')
    counter=0
    for line in fileIn:
        counter+=1
        print(line, end=' ')
        if counter==MAXLIN: break
    fileIn.close()
    return 

parser = argparse.ArgumentParser(description=\
        """Mostrar les 5,10 o 15 primereslínies del fitxer""",\
        epilog="llest!")

parser.add_argument("-n","--nlin",type=int, choices=[5,10,15],\
        help="Número de línies",dest="nlin",\
        metavar="numLines",default=10)

parser.add_argument("-v", "--verbose", action='store_true', default=False)

parser.add_argument("-f", "--file", type=str, action="append",\
        help="fitxer a processar", metavar="file", dest="fileList")#nargs='*'

args=parser.parse_args()

print(args)

# -------------------------------------------------------

MAXLIN=args.nlin
if args.fileList:
    for fitxerIn in args.fileList:
        if args.verbose: print("\n", fitxerIn, 40*"-")
        mostra_linies(fitxerIn)
        print('\n')
exit(0)
