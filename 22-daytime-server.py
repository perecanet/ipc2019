# /usr/bin/python
#-*- coding: utf-8-*-
#
# Crear un daytime client/server amb un popen de date. El server plega un cop respost.
# En aquest cas el client pot ser qualsevol eina client que simplement es connecta i
# escolta la resposta del servidor.
# El client es connecta al servidor  i aquest li retorna la data i tanca la conexió. El
# client mostra l adata rebuda i en veure que s'ha tancat la connexió també finalitza.
# El servidor engega i espera a rebre una conexió, quan l'accepta executa un Popen per
# fer un date del sistema operatiu, retorna la informació i tanca la conexió. També
# finalitza (és un servidor molt poc treballador!!).
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
from subprocess import Popen, PIPE
import argparse
HOST = ''
PORT = 50003
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print("Connected by", addr)
cmd =["date"]
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
for line in pipeData.stdout:
    line = bytes(line, 'utf-8')
    conn.send(line)
conn.close()
exit(0)