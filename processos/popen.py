# /usr/bin/python
#-*- coding: utf-8-*-
#
# --------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# --------------------------------------

# exemple-popen.py
#---------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
    """Exemple Popen""")
parser.add_argument("ruta",type=str,\
    help="directori a llistar")
args=parser.parse_args()
#--------------------------------------
command = ["ls", args.ruta]
pipeData = Popen(command, stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"))
exit(0)