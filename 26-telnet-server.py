# /usr/bin/python3
#-*- coding: utf-8-*-
# telnet-server
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2019
# -------------------------------------
#
# Pere Canet Pons
#
import sys,socket,argparse, signal
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()

HOST = ''
PORT = args.port
EOF = bytes(chr(4), 'utf-8')

def mysigusr1(signum,frame):
  print("Signal handler called with signal:", signum)
  sys.exit(0)

signal.signal(signal.SIGUSR1,mysigusr1)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    print("Addr connectada: ", addr)
    while True:
        data = conn.recv(1024)
        print('Rebut', repr(data))
        if not data: break
        pipeData = Popen(data, stdout=PIPE, stderr=PIPE, shell=True)
        for line in pipeData.stdout:
            conn.sendall(line)
        for line in pipeData.stderr:
            conn.sendall(line)
        conn.sendall(EOF)        
    conn.close()

s.close()
sys.exit(0)
