# /usr/bin/python
#-*- coding: utf-8-*-
#
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# -------------------------------------

# Exemple de programacio Objectes POO

# -------------------------------------

class UnixUser():
    """Classe UnixUser: prototipus de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    def __init__(self, l, i, g):
        "Constructor objectes UnixUser"
        self.login=l
        self.uid=i
        self.gid=g
    def show(self):
        """Mostrar les dades de l'usuari"""
        print("login: ", self.login, " uid: ", self.uid, " gid: ", self.gid)
    def sumaun(self):
        self.uid+=1
    def __str__(self):
        """Funio per retornar un string de l'objecte"""
        return "%s %d %d" % (self.login, self.uid, self.gid)
exit(0)