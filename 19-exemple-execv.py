# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-execv.py
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, os
print("començament programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    print("Pid fill", pid)
    exit(0)
print("Programa fill", os.getpid(), pid)
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"]) #el proces passa a executar un ls de l'arrel
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-ls", "/") #a tots, path és el mateix que l'arg0
#os.execlp("ls", "ls", "-ls", "/") # no cal ruta absoluta, utilitza el PATH
#os.execve("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"], {"nom":"joan","edat":25}) #cal donar l'environment
#os.execvp("uname", ["uname","-a"])
os.execv("/usr/bin/bash", ["/usr/bin/bash", "~/Documents/m06/ipc2019/show.sh"])
os.execle()
print("adeu") #no s'arriba a executar
sys.exit(0)
