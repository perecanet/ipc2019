#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Ídem exercici anterior, generar un daytime-server que accepta múltiples clients
# correlatius, és a dir, un un cop finalitzat l'anteior: One2one.
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
from subprocess import Popen, PIPE

HOST = ''
PORT = 50002
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    cmd = ["date"]
    pipeData = Popen(cmd, stdout=PIPE)
    for line in pipeData.stdout:
        line = bytes(line, 'utf-8')
        conn.send(line)
    conn.close()
