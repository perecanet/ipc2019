# exemple-signal.py
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, os

print("començant programa principal")
print("PID: pare ", os.getpid())

pid=os.fork()
if pid != 0:
    os.wait()
    print("Programa Pare", os.getpid(), pid)
else:
    print("Programa fill", os.getpid(), pid)

print("Adeu")
sys.exit(0)