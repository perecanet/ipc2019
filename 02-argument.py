# /usr/bin/python
#-*- coding: utf-8-*-

import argparse
parser = argparse.ArgumentParser(description="programa exemple de processar arguments",prog="02-arguments.py",epilog="adeu")
parser.add_argument("-e", "--edat", type=int, dest="useredat", help="edat a processar")
parser.add_argument("-f", "--fit", dest="fitxer", type=str, help="fitxer a processar", metavar="fileIn")
args = parser.parse_args()
print(parser)
print(args)
exit(0)

