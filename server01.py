# /usr/bin/python3
#-*- coding: utf-8-*-
#
# Pere Canet Pons
# isx41747980
# -------------------------------------
# Examen 10
# IPC Pràctic
# HISX2
# 06/02/2020
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# -------------------------------------
# servidor One2One que respon al client les comandes ps ax, netstat -puta o uname -a,
# segons el que aquest demani

import sys, socket, os, signal, argparse
from subprocess import Popen, PIPE

# Validació dels arguments
parser = argparse.ArgumentParser(description='Executa la comanda demanada pel user')
parser.add_argument("-p", "--port", type=int, help="Port on escoltar",\
     default=44444, dest="port")
parser.add_argument("-d","--debug",action='store_true',default=False)
args = parser.parse_args()

# -------------------
# Definició de les constants host, port i myeof i de la variable l_connexions
# (hi guardem el registre de connexions al servidor)
PORT = args.port
HOST = ''
l_connexions = []

# -------------------
# Definició dels senyals

# Llistat de connexions
def mysigusr1(signum,frame):
    global l_connexions
    print("Signal handler called with signal:", signum)
    print("Llista de totes les connexions establertes fins ara: ", l_connexions)
    exit(0)

# Numero total de connexions
def mysigusr2(signum, frame):
    global l_connexions
    print("Signal handler called with signal:", signum)
    print("Numero total de connexions rebudes: ", len(l_connexions))
    exit(0)

# Llistat i numero total de connexions
def mysigterm(signum, frame):
    global l_connexions
    print("Signal handler called with signal:", signum)
    print("Numero total de connexions rebudes: ", len(l_connexions))
    print("Llistat d'aquestes: ", l_connexions)
    exit(0)

signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

# -------------------
# Connexió de tipus TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Permetre reutilitzar l'adreça tot i que estigui bloquejada per accions anteriors
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Lligar IP del host i port
s.bind((HOST,PORT))

# Començar a escoltar
s.listen(1)

# Per cada client
while True: 
    # Escoltar fins que hi hagi una connexió
    conn, addr = s.accept()
    print("Connected by", addr)

    # Afegir aquesta a la llista de connexions
    l_connexions.append(addr)

    # En cas d'estar en mode debug, mostrar per pantalla
    if args.debug:
	    print ("Connected by: ", addr[0])

    while True:
        # Rebre la comanda del client
        command = conn.recv(1024)
        
        # Si hi ha el mode debug activat
        if args.debug:
            print("Comanda: ", command)
        
        # Si no es reben dades, connexió finalitzada
        if not command:

            print("Connexió amb ", addr[0], " finalitzada")
            break
        
        # Comprovar quina comanda s'ha rebut
            #Cas de processos
        if str(command)[2:11] == 'processos':
            
            command = 'ps ax'

        elif str(command)[2:7] == 'ports':
            
            command = 'netstat -puta'

        else:

            command = 'uname -a'

        # Execució de la comanda
        pipeData = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)

        # Enviament de cada resultat del PIPE.stdout
        for line in pipeData.stdout:

            #Mode debug
            if args.debug:
                print("Enviant: ", line)

            conn.send(line)

        # Enviament de cada resultat del PIPE.stderr
        for line in pipeData.stderr:

            #Mode debug
            if args.debug:
                print("Enviant: ", line)

            conn.send(line)
        #Enviar senyal de final de comanda
        conn.send(b'\x04')
    
    conn.close()

sys.exit(0)